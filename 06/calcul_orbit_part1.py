

class Node:
    def __init__(self, name, parent_node = None):
        self.parent_node = parent_node

    @property
    def count_ancestors(self):
        current_parent = self.parent_node
        nb_ancestors = 0
        while current_parent is not None:
            nb_ancestors += 1
            current_parent = current_parent.parent_node
        return nb_ancestors

    def __str__(self):
        return f'{self.name}:{self.count_ancestors}'


NODES = dict()
def parseur(lines):
    lines = ( l.strip() for l in lines )
    lines = filter(None, lines)
    lines = ( l.split(')') for l in lines )
    for line in lines:
        parent_name, name = line
        if parent_name in NODES:
            parent_node = NODES[parent_name]
        else:
            parent_node = Node(parent_name)
            NODES[parent_name] = parent_node
        if name in NODES:
            node = NODES[name]
            node.parent_node = parent_node
        else:
            node = Node(name, parent_node)
            NODES[name] = node

def compute():
    nodes = ( node.count_ancestors for node in NODES.values() )
    print(sum(nodes))


def example():
    parseur("""COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L""".split('\n'))
    compute()

def play():
    with open('input.txt') as fd:
        lines = fd.readlines()
    parseur(lines)
    compute()

if __name__ == '__main__':
    #example()
    play()
    
