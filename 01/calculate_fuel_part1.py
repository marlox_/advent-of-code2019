import math
import typing

def get_fuel_level(module_mass: int) -> int:
    fuel_level = module_mass / 3.
    fuel_level = math.floor(fuel_level)
    fuel_level -= 2
    return fuel_level

def get_full_fuel_level(modules_mass: typing.Iterable[int]) -> int:
    return sum(map(get_fuel_level, modules_mass))

def test(module_mass: int, expected_fuel_level: int):
    fuel_level = get_fuel_level(module_mass)
    print(f"get_fuel_level({module_mass})={fuel_level} =? {expected_fuel_level} : ", end='')
    if fuel_level == expected_fuel_level:
        print('ok')
    else:
        print("fail")

def example():
    test(12, 2)
    test(14, 2)
    test(1969, 654)
    test(100756, 33583)
    print(get_full_fuel_level([12,14]))
    print(get_full_fuel_level([12,14,1969, 100756]))

def play():
    with open('input.txt') as fd:
        lines = fd.readlines()
    lines = ( l.strip() for l in lines )
    lines = map(int, lines)
    print(get_full_fuel_level(lines))

if __name__ == '__main__':
    #example()
    play()
    
