# vim:set foldmarker={{,}} foldmethod=marker number:
import abc
import collections
import enum
import logging
import itertools
import unittest
import typing

###### LOGGING {{
LOGGER = logging.getLogger('proc')
""" Pour gérer les aspects communs aux différents canaux de journalisation
"""
MEM_LOGGER = logging.getLogger('proc.mem')
""" Messages relatifs à la manipulation de la mémoire
"""
SIG_LOGGER = logging.getLogger('proc.signal')
""" Affiche les messages relatifs aux signaux transmis entre processeurs
"""
ASM_LOGGER = logging.getLogger('proc.asm')
""" Affiche les instructions exécutées par le processeur
Note: le niveau DEBUG permet de faire du pas à pas
"""
PROC_LOGGER = logging.getLogger('proc.mgmt')
""" Affiche les modifications des fils d’exécution des processus
"""
def init_logging():
    """ Paramètre la journalisation

    Notament: les niveaux, le format et les sorties
    """
    LOGGER.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(name)s:%(levelname)s:%(message)s')
    MEM_LOGGER.setLevel(logging.DEBUG)
    SIG_LOGGER.setLevel(logging.DEBUG)
    ASM_LOGGER.setLevel(logging.INFO)
    PROC_LOGGER.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.WARNING)
    stream_handler.setFormatter(formatter)
    LOGGER.addHandler(stream_handler)

###### }} end LOGGING

###### PROGRAM SEGMENT {{

Opcode = typing.NewType('Opcode', int)
""" Élément d’un segment de pseudocode
"""

CursorOffset = typing.NewType('CursorOffset', int)
""" Position relative du curseur dans un segment de pseudocode
"""

#Cursor = typing.NewType('Cursor', int)
class Cursor: # {{
    """ Position absolute du curseur dans un segment de pseudocode
    """
    def __init__(self, cursor: int):
        self.cursor = cursor

    def __add__(self, cursor_offset: CursorOffset) -> 'Cursor':
        return Cursor(self.cursor + cursor_offset)

    def __int__(self) -> int:
        return self.cursor

    def __repr__(self) -> str:
        return repr(self.cursor)

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Cursor):
            return NotImplemented
        return self.cursor == other.cursor
# }} end Cursor

#Intcode = typing.List[Opcode]
class Intcode: # {{
    """ Segment de pseudocode

        Le segment de pseudocode a une taille fixe
    """

    @classmethod
    def fromstr(cls, intcode_raw: str) -> 'Intcode':
        """ Convertit une chaine d’entier séparé par ',' en un segment de pseudocode
        """
        return cls(list(map(Opcode, map(int, intcode_raw.split(',')))))

    def __init__(self, intcode: typing.List[Opcode]):
        self.intcode = intcode
        self.length = len(self.intcode)

    def ensure_access(self, cursor: Cursor):
        if cursor.cursor >= self.length:
            MEM_LOGGER.warning(f'Extension of intcode from {self.length} with {(cursor.cursor - len(self.intcode) + 1)}')
            self.intcode.extend([Opcode(0)] * (cursor.cursor + 1 - self.length))
            self.length = len(self.intcode)

    def __getitem__(self, cursor: Cursor) -> Opcode:
        self.ensure_access(cursor)
        return self.intcode[cursor.cursor]

    def __setitem__(self, cursor: Cursor, new_value: Opcode):
        self.ensure_access(cursor)
        try:
            MEM_LOGGER.debug(f'Write opcode {new_value} to cursor {cursor}')
            self.intcode[cursor.cursor] = new_value
        except IndexError:
            MEM_LOGGER.error(f'len intcode is {len(self.intcode)}. Cursor: {cursor.cursor}')
            raise

    def __str__(self):
        return repr(self.intcode)

    def copy(self) -> 'Intcode':
        return Intcode(self.intcode[:])
# }} end Intcode

###### }} end PROGRAM SEGMENT

###### OPERATORS {{
class BaseOperator(metaclass=abc.ABCMeta): # {{
    NB_ARGS = 0
    OPERATOR = '?'

    def __init__(self, processor: 'IntcodeProcessor'):
        self.processor = processor
        self.modes = [0] * self.__class__.NB_ARGS
        self.self_instruction_is_modified = False

    def is_position_mode(self, offset: CursorOffset) -> bool:
        return self.modes[offset - 1] == 0

    def is_relative_mode(self, offset: CursorOffset) -> bool:
        return self.modes[offset - 1] == 2

    def get_affected_cursor(self, param_offset: CursorOffset) -> Cursor:
        value_under_cursor_offset = self.processor.intcode[self.processor.cursor + param_offset]
        if self.is_position_mode(param_offset):
            affected_cursor = Cursor(value_under_cursor_offset)
        elif self.is_relative_mode(param_offset):
            affected_cursor = self.processor.relative_base + CursorOffset(value_under_cursor_offset)
        else:
            affected_cursor = self.processor.cursor + param_offset
        return affected_cursor

    def get_argvalue(self, offset: CursorOffset) -> Opcode:
        return self.processor.intcode[self.get_affected_cursor(offset)]

    def set_argvalue(self, offset: CursorOffset, new_opcode: Opcode):
        affected_cursor = self.get_affected_cursor(offset)
        self.processor.intcode[affected_cursor] = new_opcode
        if affected_cursor == self.processor.cursor:
            self.self_instruction_is_modified = True

    @property
    def param1(self) -> Opcode:
        return self.get_argvalue(CursorOffset(1))

    @param1.setter
    def param1(self, new_value: Opcode):
        self.set_argvalue(CursorOffset(1), new_value)

    @property
    def param2(self) -> Opcode:
        return self.get_argvalue(CursorOffset(2))

    @param2.setter
    def param2(self, new_value: Opcode):
        self.set_argvalue(CursorOffset(2), new_value)

    @property
    def param3(self) -> Opcode:
        return self.get_argvalue(CursorOffset(3))

    @param3.setter
    def param3(self, new_value: Opcode):
        self.set_argvalue(CursorOffset(3), new_value)

    @abc.abstractmethod
    def process(self):
        pass

    def update_cursor(self):
        if not self.self_instruction_is_modified and self.processor.state == ProcessorState.RUNNING:
            self.processor.cursor += CursorOffset(self.__class__.NB_ARGS + 1)

    def argvalue_debug(self,
                       offset: CursorOffset,
                       with_value: bool = True
                       ) -> str:
        ret: typing.List[str] = list()
        target_cursor = self.processor.cursor + offset
        if self.is_position_mode(offset):
            ret = [
                f'*p{offset}',
                f'*[{target_cursor}]',
            ]
        elif self.is_relative_mode(offset):
            ret = [
                f'*[B+p{offset}]',
                f'[{self.processor.relative_base}+{self.processor.intcode[target_cursor]}]',
            ]
        else:
            ret = [
                f'p{offset}',
            ]
        ret.append(f'[{self.get_affected_cursor(offset)}]')
        if with_value:
            ret.append(str(self.get_argvalue(offset)))
        return '='.join(ret)

    @property
    def p1_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(1))

    @property
    def p1_set_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(1), False)

    @property
    def p2_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(2))

    @property
    def p2_set_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(2), False)

    @property
    def p3_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(3))

    @property
    def p3_set_dbg(self) -> str:
        return self.argvalue_debug(CursorOffset(3), False)

    def __str__(self):
        return 'not documented'
# }} end BaseOperator

# BINARY MATH OPERATORS {{
class BinaryMathOperator(BaseOperator, metaclass=abc.ABCMeta): # {{
    NB_ARGS = 3

    def __str__(self):
        return f'{self.p3_set_dbg} <= {self.p1_dbg} {self.__class__.OPERATOR} {self.p2_dbg}'
# }} end BinaryMathOperator

class AddOperator(BinaryMathOperator): # {{
    OPERATOR = '+'

    def process(self):
        self.param3 = self.param1 + self.param2
# }} end AddOperator

class MulOperator(BinaryMathOperator): # {{
    OPERATOR = '×'

    def process(self):
        self.param3 = self.param1 * self.param2
# }} end MulOperator
## }} end BINARY MATH OPERATORS

## I/O OPERATORS {{
class InputOperator(BaseOperator): # {{
    NB_ARGS = 1

    def process(self):
        if self.processor.input:
            self.param1 = Opcode(self.processor.input.read())
        else:
            self.processor.state = ProcessorState.BLOCKED

    def __str__(self):
        if self.processor.input:
            return f'{self.p1_set_dbg} <= {self.processor.input.display()} (input)'
        return f'{self.p1_set_dbg} (hang on input)'
# }} end InputOperator

class OutputOperator(BaseOperator): # {{
    NB_ARGS = 1

    def process(self):
        self.processor.output.write(Signal(self.param1))

    def __str__(self):
        return f'output {self.p1_dbg}'
# }} end OutputOperator
## }} end I/O OPERATORS

## BRANCHING OPERATORS {{
class JumpIfOperator(BaseOperator, metaclass=abc.ABCMeta): # {{
    NB_ARGS = 2

    def process(self):
        pass

    def update_cursor(self):
        if self.jump_on_true():
            self.processor.cursor = Cursor(self.param2)
        else:
            super().update_cursor()

    @abc.abstractmethod
    def jump_on_true(self) -> bool:
        pass

    def __str__(self):
        return f'jmp {self.p2_dbg} if {self.p1_dbg} {self.__class__.OPERATOR} 0'
# }} end JumpIfOperator

class JumpIfTrueOperator(JumpIfOperator): # {{
    OPERATOR = '!='

    def jump_on_true(self) -> bool:
        return self.param1 != Opcode(0)
# }} end JumpIfTrueOperator

class JumpIfFalseOperator(JumpIfOperator): # {{
    OPERATOR = '=='

    def jump_on_true(self) -> bool:
        return self.param1 == Opcode(0)
# }} end JumpIfFalseOperator
## }} end BRANCHING OPERATORS

## COMPARATOR OPERATORS {{
class ComparatorOperator(BaseOperator, metaclass=abc.ABCMeta): # {{
    NB_ARGS = 3

    def process(self):
        if self.cmp_is_true():
            self.param3 = Opcode(1)
        else:
            self.param3 = Opcode(0)

    @abc.abstractmethod
    def cmp_is_true(self) -> bool:
        pass

    def __str__(self):
        return (f'{self.p3_set_dbg} <= 1 if {self.p1_dbg}'
                f'{self.__class__.OPERATOR} {self.p2_dbg} else 0')
# }} end ComparatorOperator

class LessThanOperator(ComparatorOperator): # {{
    OPERATOR = '<'

    def cmp_is_true(self) -> bool:
        return self.param1 < self.param2
# }} end LessThanOperator

class EqualOperator(ComparatorOperator): # {{
    OPERATOR = '=='
    def cmp_is_true(self) -> bool:
        return self.param1 == self.param2
# }} end EqualOperator
## }} end COMPARATOR OPERATORS

class AdjustRelativeBaseOperator(BaseOperator): # {{
    NB_ARGS = 1

    def process(self):
        self.processor.relative_base += CursorOffset(self.param1)

    def __str__(self):
        return f'adjust relative base ({self.processor.relative_base}) with {self.p1_dbg}'
# }} end SetRelativeBaseOperator

class StopOperator(BaseOperator): # {{
    NB_ARGS = 0

    def process(self):
        self.processor.state = ProcessorState.STOPPED

    def __str__(self):
        return 'stop'
# }} end StopOperator

OPCODES = { # {{
    Opcode(1): AddOperator,
    Opcode(2): MulOperator,
    Opcode(3): InputOperator,
    Opcode(4): OutputOperator,
    Opcode(5): JumpIfTrueOperator,
    Opcode(6): JumpIfFalseOperator,
    Opcode(7): LessThanOperator,
    Opcode(8): EqualOperator,
    Opcode(9): AdjustRelativeBaseOperator,
    Opcode(99): StopOperator,
}
# }}

###### }} end OPERATORS

###### SIGNAL MANAGEMENT {{
Signal = typing.NewType('Signal', int)
Settings = typing.List[Signal]

def settings_from_str(settings_raw: str) -> Settings:
    return list(map(Signal, map(int, settings_raw.split(','))))


class SignalReceiver: # {{
    def __init__(self):
        self.signal_stack = collections.deque()

    def write(self, signal: Signal):
        self.signal_stack.append(signal)

    def read(self) -> Signal:
        return self.signal_stack.popleft()

    def display(self) -> Signal:
        signal = self.signal_stack.popleft()
        self.signal_stack.appendleft(signal)
        return signal

    def __bool__(self) -> bool:
        if self.signal_stack:
            return True
        return False
# }} end SignalReceiver

class SignalEmitter: # {{
    def __init__(self, signal_dispatcher: 'SignalDispatcher'):
        self.signal_dispatcher = signal_dispatcher

    def write(self, signal: Signal):
        self.signal_dispatcher.write(signal)
# }} end SignalEmitter

class NullSignalEmitter(SignalEmitter): # {{
    def __init__(self, processor: 'IntcodeProcessor'):
        self.processor = processor
        self.warned = False

    def write(self, signal: Signal):
        if not self.warned:
            SIG_LOGGER.warning(f'Warning: send signal to dev null from {self.processor}')
            self.warned = True
        print(signal)
# }} end NullSignalEmitter


class SignalDispatcher: # {{
    @classmethod
    def connect(cls,
                output_processor: 'IntcodeProcessor',
                input_processor: 'IntcodeProcessor'
                ) -> 'SignalDispatcher':
        dispatcher = SignalDispatcher()
        dispatcher.connect_to_proc_input(input_processor)
        dispatcher.connect_to_proc_output(output_processor)
        return dispatcher

    def __init__(self):
        self.intcode_processor_receivers = list()

    def connect_to_proc_input(self, intcode_processor: 'IntcodeProcessor'):
        self.intcode_processor_receivers.append(intcode_processor)

    def connect_to_proc_output(self, intcode_processor: 'IntcodeProcessor'):
        intcode_processor.output = SignalEmitter(self)

    @property
    def receivers(self) -> str:
        return ', '.join(map(lambda proc: proc.name, self.intcode_processor_receivers))

    def write(self, signal: Signal):
        SIG_LOGGER.debug(f'Send {signal} to {self.receivers}')
        for intcode_processor in self.intcode_processor_receivers:
            intcode_processor.input.write(signal)
            if intcode_processor.state == ProcessorState.BLOCKED:
                intcode_processor.state = ProcessorState.WAITING
# }} end SignalDispatcher

###### }} end SIGNAL MANAGEMENT

###### PROCESSOR {{

class ProcessorState(enum.Enum): #{{
    RUNNING = 0
    WAITING = 1
    BLOCKED = 2
    STOPPED = 3
# }}

class IntcodeProcessor: # {{
    def __init__(self, name, intcode: Intcode):
        self.name = name
        self.intcode = intcode
        self.cursor = Cursor(0)
        self.input = SignalReceiver()
        self.output: SignalEmitter = NullSignalEmitter(self)
        self.relative_base = Cursor(0)
        self._state = ProcessorState.WAITING

    def __str__(self):
        return f'#{self.name}'

    @property
    def state(self) -> ProcessorState:
        return self._state

    @state.setter
    def state(self, new_state: ProcessorState):
        PROC_LOGGER.debug(f'State of {self} change : {self._state.name} -> {new_state.name}')
        self._state = new_state

    def process(self):
        assert self.state == ProcessorState.WAITING, \
                "{self}.state is {self.state.name} != WAITING : cannot process"
        self.state = ProcessorState.RUNNING
        while self.state == ProcessorState.RUNNING:
            self.step()

    def step(self):
        opcode = self.intcode[self.cursor]
        modes = opcode // 100
        opcode = opcode % 100
        operator = OPCODES[opcode]
        operator = operator(self)
        if modes > 0:
            for idx, digit in enumerate(reversed(str(modes))):
                operator.modes[idx] = int(digit)
        ASM_LOGGER.info(f'{self} @{self.cursor}={opcode} {operator}')
        if ASM_LOGGER.level <= logging.DEBUG:
            input()
        operator.process()
        operator.update_cursor()
# }} end IntcodeProcessor

class IntcodeProcessorScheduler: # {{
    @classmethod
    def fromstr(cls, intcode_raw: str, settings_raw: str):
        intcode = Intcode.fromstr(intcode_raw)
        settings = settings_from_str(settings_raw)
        return cls(intcode, settings)

    def __init__(self, intcode: Intcode, settings: Settings):
        self.processors: typing.List[IntcodeProcessor] = list()
        self.stdout = SignalReceiver()
        for num, setting in enumerate(settings):
            processor = IntcodeProcessor(chr(65 + num), intcode.copy())
            processor.input.write(setting)
            self.processors.append(processor)
        self.make_connections()

    def make_connections(self):
        for proc1, proc2 in zip(self.processors[:-1], self.processors[1:]):
            SignalDispatcher.connect(proc1, proc2)
        self.processors[-1].output = self.stdout
        self.processors[0].input.write(0)

    def get_next_waiting_process(self) -> typing.Optional[IntcodeProcessor]:
        for processor in self.processors:
            if processor.state == ProcessorState.WAITING:
                return processor
        return None

    def process(self) -> Signal:
        processor = self.get_next_waiting_process()
        while processor:
            PROC_LOGGER.debug(f'Exec {processor}')
            processor.process()
            processor = self.get_next_waiting_process()
        return self.final_output()

    def final_output(self) -> Signal:
        return self.stdout.read()
# }} end IntcodeProcessorScheduler

class IntcodeProcessorScheduler2(IntcodeProcessorScheduler): # {{
    def make_connections(self):
        super().make_connections()
        SignalDispatcher.connect(self.processors[-1], self.processors[0])

    def final_output(self) -> Signal:
        return self.processors[0].input.read()
# }} end IntcodeProcessorScheduler2

###### }} PROCESSOR

def bruteforce( # {{
        intcode_raw: str,
        settings_raw: str = "0,1,2,3,4",
        scheduler=IntcodeProcessorScheduler
        ) -> typing.Iterator[Signal]:
    intcode = Intcode.fromstr(intcode_raw)
    settings_value = settings_from_str(settings_raw)
    for settings in itertools.permutations(settings_value):
        factory = scheduler(intcode, settings)
        yield factory.process()
# }} end bruteforce

# TESTS {{
class CheckMixin(unittest.TestCase): # {{
    def cmp_intcode(self, intcode1: Intcode, intcode2: Intcode, length: int):
        for idx in map(Cursor, range(length)):
            self.assertEqual(intcode1[idx], intcode2[idx],
                             f'fail to cmp {intcode1} and {intcode2} at pos {idx}')

    def step_intcode(self, input_intcode: Intcode, expected_output_intcode: Intcode, length: int):
        processor = IntcodeProcessor('', input_intcode)
        processor.state = ProcessorState.RUNNING
        processor.step()
        self.cmp_intcode(processor.intcode, expected_output_intcode, length)

    def step_cursor_pos(self, input_intcode: Intcode, expected_cursor_pos: Cursor):
        processor = IntcodeProcessor('', input_intcode)
        processor.state = ProcessorState.RUNNING
        processor.step()
        self.assertEqual(processor.cursor, expected_cursor_pos, input_intcode)

    def process_intcode(self,
                        input_intcode: Intcode,
                        expected_output_intcode: Intcode,
                        length: int):
        processor = IntcodeProcessor('', input_intcode)
        processor.process()
        self.cmp_intcode(processor.intcode, expected_output_intcode, length)

# }} end CheckMixin

class TestSegment(CheckMixin): # {{
    def test_cmp(self):
        loaded_intcode = Intcode.fromstr('1,0,0,0,99')
        loaded_intcode2 = Intcode.fromstr('1,0,0,0,99')
        self.cmp_intcode(loaded_intcode, loaded_intcode2, 5)

    def test_segment_loading(self):
        loaded_intcode = Intcode.fromstr('1,0,3,99')
        loaded_intcode2 = Intcode([Opcode(1), Opcode(0), Opcode(3), Opcode(99)])
        self.cmp_intcode(loaded_intcode, loaded_intcode2, 4)

    def test_cursor_move(self):
        loaded_intcode = Intcode.fromstr('2,4,4,5,99')
        cursor = Cursor(0)
        self.assertEqual(loaded_intcode[cursor], Opcode(2))
        cursor += CursorOffset(3)
        self.assertEqual(loaded_intcode[cursor], Opcode(5))
        self.assertEqual(loaded_intcode[cursor + CursorOffset(1)], Opcode(99))

    def test_opcode_writing(self):
        loaded_intcode = Intcode.fromstr('1,1,1,4,99,5,6,0,99')
        loaded_intcode[Cursor(1)] = Opcode(20)
        loaded_intcode[Cursor(8)] = Opcode(30)
        expected_intcode = Intcode.fromstr('1,20,1,4,99,5,6,0,30')
        self.cmp_intcode(loaded_intcode, expected_intcode, 8)

    def test_copy(self):
        loaded_intcode = Intcode.fromstr('1,0,0,0,99')
        copied_intcode = loaded_intcode.copy()
        loaded_intcode[Cursor(1)] = 5
        copied_intcode[Cursor(3)] = 3
        expected_intcode = Intcode.fromstr('1,5,0,0,99')
        self.cmp_intcode(loaded_intcode, expected_intcode, 5)
        expected_intcode = Intcode.fromstr('1,0,0,3,99')
        self.cmp_intcode(copied_intcode, expected_intcode, 5)

# }} end TestSegment

class TestMathOperator(CheckMixin): # {{
    def test_add_direct(self):
        loaded_intcode = Intcode.fromstr('11101,3,0,5')
        expected_intcode = Intcode.fromstr('11101,3,0,3')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('11101,3,3,2')
        expected_intcode = Intcode.fromstr('11101,3,3,6')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_mul_direct(self):
        loaded_intcode = Intcode.fromstr('11102,3,0,5')
        expected_intcode = Intcode.fromstr('11102,3,0,0')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('11102,4,3,100')
        expected_intcode = Intcode.fromstr('11102,4,3,12')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_add_with_ref(self):
        loaded_intcode = Intcode.fromstr('1,3,0,5,99,0')
        expected_intcode = Intcode.fromstr('1,3,0,5,99,6')
        self.step_intcode(loaded_intcode, expected_intcode, 6)
        loaded_intcode = Intcode.fromstr('1,3,3,2')
        expected_intcode = Intcode.fromstr('1,3,4,2')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_mul_with_ref(self):
        loaded_intcode = Intcode.fromstr('2,3,0,5,30,0')
        expected_intcode = Intcode.fromstr('2,3,0,5,30,10')
        self.step_intcode(loaded_intcode, expected_intcode, 6)
        loaded_intcode = Intcode.fromstr('2,3,0,3')
        expected_intcode = Intcode.fromstr('2,3,0,6')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_cursor_change(self):
        loaded_intcode = Intcode.fromstr('11101,3,0,5')
        self.step_cursor_pos(loaded_intcode, Cursor(4))
        loaded_intcode = Intcode.fromstr('1,3,0,4,2')
        self.step_cursor_pos(loaded_intcode, Cursor(4))

    def test_cursor_nochange(self):
        loaded_intcode = Intcode.fromstr('1,3,0,0,2')
        self.step_cursor_pos(loaded_intcode, Cursor(0))
        loaded_intcode = Intcode.fromstr('2,3,0,0,2')
        self.step_cursor_pos(loaded_intcode, Cursor(0))
# }} end TestMathOperator

class TestJumpOperator(CheckMixin): # {{
    def test_true_direct(self):
        loaded_intcode = Intcode.fromstr('1105,6,5,1,2,3,4')
        self.step_cursor_pos(loaded_intcode, Cursor(5))
        loaded_intcode = Intcode.fromstr('1105,0,3,0,4,2')
        self.step_cursor_pos(loaded_intcode, Cursor(3))

    def test_false_direct(self):
        loaded_intcode = Intcode.fromstr('1106,0,4,0,2,9,8,7')
        self.step_cursor_pos(loaded_intcode, Cursor(4))
        loaded_intcode = Intcode.fromstr('1106,7,1,3,2,9')
        self.step_cursor_pos(loaded_intcode, Cursor(3))

    def test_true_with_ref(self):
        loaded_intcode = Intcode.fromstr('5,6,4,1,2,0,4,0,8')
        self.step_cursor_pos(loaded_intcode, Cursor(2))
        loaded_intcode = Intcode.fromstr('5,3,6,0,4,2,8,9,6')
        self.step_cursor_pos(loaded_intcode, Cursor(3))

    def test_false_with_ref(self):
        loaded_intcode = Intcode.fromstr('6,6,4,0,1,6,0,7')
        self.step_cursor_pos(loaded_intcode, Cursor(1))
        loaded_intcode = Intcode.fromstr('6,5,1,3,2,9')
        self.step_cursor_pos(loaded_intcode, Cursor(3))
# }} end TestJumpOperator

class TestCmpOperator(CheckMixin): # {{
    def test_eq_direct(self):
        loaded_intcode = Intcode.fromstr('11108,3,3,5')
        expected_intcode = Intcode.fromstr('11108,3,3,1')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('11108,3,4,5')
        expected_intcode = Intcode.fromstr('11108,3,4,0')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_lt_direct(self):
        loaded_intcode = Intcode.fromstr('11107,3,3,5')
        expected_intcode = Intcode.fromstr('11107,3,3,0')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('11107,3,4,5')
        expected_intcode = Intcode.fromstr('11107,3,4,1')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_eq_with_ref(self):
        loaded_intcode = Intcode.fromstr('108,2,3,2')
        expected_intcode = Intcode.fromstr('108,2,1,2')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('8,2,3,1')
        expected_intcode = Intcode.fromstr('8,0,3,1')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_lt_with_ref(self):
        loaded_intcode = Intcode.fromstr('7,3,0,2')
        expected_intcode = Intcode.fromstr('7,3,1,2')
        self.step_intcode(loaded_intcode, expected_intcode, 4)
        loaded_intcode = Intcode.fromstr('7,2,1,0')
        expected_intcode = Intcode.fromstr('1,2,1,0')
        self.step_intcode(loaded_intcode, expected_intcode, 4)

    def test_cursor_change(self):
        loaded_intcode = Intcode.fromstr('11108,3,0,5')
        self.step_cursor_pos(loaded_intcode, Cursor(4))
        loaded_intcode = Intcode.fromstr('7,3,0,4,2')
        self.step_cursor_pos(loaded_intcode, Cursor(4))

    def test_cursor_nochange(self):
        loaded_intcode = Intcode.fromstr('7,3,4,0,2')
        self.step_cursor_pos(loaded_intcode, Cursor(0))
        loaded_intcode = Intcode.fromstr('108,0,1,0')
        self.step_cursor_pos(loaded_intcode, Cursor(0))
# }} end TestCmpOperator

class TestFlowOperator(CheckMixin): # {{
    def test_stop_intcode_nochange(self):
        loaded_intcode = Intcode.fromstr('99')
        self.step_intcode(loaded_intcode, loaded_intcode, 1)

    def test_stop_state_change(self):
        loaded_intcode = Intcode.fromstr('99')
        processor = IntcodeProcessor('', loaded_intcode)
        processor.state = ProcessorState.RUNNING
        processor.step()
        self.assertEqual(processor.state, ProcessorState.STOPPED)

    def test_input_empty(self):
        loaded_intcode = Intcode.fromstr('3,1')
        processor = IntcodeProcessor('', loaded_intcode)
        processor.state = ProcessorState.RUNNING
        processor.step()
        self.assertEqual(processor.state, ProcessorState.BLOCKED)
        self.assertEqual(processor.cursor, Cursor(0))
# }} end TestFlowOperator

class TestIOOperator(CheckMixin): # {{
    def test_input_direct(self):
        loaded_intcode = Intcode.fromstr('103,2,2,5,7')
        processor = IntcodeProcessor('', loaded_intcode)
        processor.state = ProcessorState.RUNNING
        processor.input.write(Signal(8))
        processor.step()
        self.assertEqual(processor.state, ProcessorState.RUNNING)
        self.assertEqual(processor.cursor, Cursor(2))
        expected_intcode = Intcode.fromstr('103,8,2,5,7')
        self.cmp_intcode(loaded_intcode, expected_intcode, 5)

    def test_input_with_ref(self):
        loaded_intcode = Intcode.fromstr('3,4,2,5,7')
        processor = IntcodeProcessor('', loaded_intcode)
        processor.state = ProcessorState.RUNNING
        processor.input.write(Signal(8))
        processor.step()
        self.assertEqual(processor.state, ProcessorState.RUNNING)
        self.assertEqual(processor.cursor, Cursor(2))
        expected_intcode = Intcode.fromstr('3,4,2,5,8')
        self.cmp_intcode(loaded_intcode, expected_intcode, 5)
# }} end TestIOOperator

class TestSimpleSequences(CheckMixin): # {{
    def test_two_operations(self):
        loaded_intcode = Intcode.fromstr('2,3,0,3,99')
        expected_intcode = Intcode.fromstr('2,3,0,6,99')
        self.process_intcode(loaded_intcode, expected_intcode, 5)
        loaded_intcode = Intcode.fromstr('2,4,4,5,99,0')
        expected_intcode = Intcode.fromstr('2,4,4,5,99,9801')
        self.process_intcode(loaded_intcode, expected_intcode, 6)
        loaded_intcode = Intcode.fromstr('1101,100,-1,4,0')
        expected_intcode = Intcode.fromstr('1101,100,-1,4,99')
        self.process_intcode(loaded_intcode, expected_intcode, 5)


    def test_three_operations(self):
        loaded_intcode = Intcode.fromstr('1,1,1,4,99,5,6,0,99')
        expected_intcode = Intcode.fromstr('30,1,1,4,2,5,6,0,99')
        self.process_intcode(loaded_intcode, expected_intcode, 9)
        loaded_intcode = Intcode.fromstr('1,9,10,3,2,3,11,0,99,30,40,50')
        expected_intcode = Intcode.fromstr('3500,9,10,70,2,3,11,0,99,30,40,50')
        self.process_intcode(loaded_intcode, expected_intcode, 12)
# }} end TestSimpleSequences

class TestSimpleScheduler(unittest.TestCase): # {{
    def scheduler_test(self, intcode: str, settings: str, expected_result: int):
        factory = IntcodeProcessorScheduler.fromstr(intcode, settings)
        result = factory.process()
        self.assertEqual(result, expected_result, intcode)

    def test_scheduler(self):
        intcode = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
        self.scheduler_test(intcode, "4,3,2,1,0", 43210)
        intcode = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
        self.scheduler_test(intcode, "0,1,2,3,4", 54321)
        intcode = ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,"
                   "33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0")
        self.scheduler_test(intcode, "1,0,4,3,2", 65210)
# }} end TestSimpleScheduler

class TestBruteforceScheduler(unittest.TestCase): # {{
    def scheduler_bruteforce1_test(self, intcode: str, expected_result: int):
        result = max(bruteforce(intcode))
        self.assertEqual(result, expected_result, intcode)

    def scheduler_bruteforce2_test(self, intcode: str, expected_result: int):
        result = max(bruteforce(intcode, "5,6,7,8,9", IntcodeProcessorScheduler2))
        self.assertEqual(result, expected_result, intcode)

    def test_scheduler_bruteforce(self):
        intcode = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
        self.scheduler_bruteforce1_test(intcode, 43210)
        intcode = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
        self.scheduler_bruteforce1_test(intcode, 54321)
        intcode = ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,"
                   "1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0")
        self.scheduler_bruteforce1_test(intcode, 65210)

    def test_scheduler_bruteforce2(self):
        intcode = ("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,"
                   "4,27,1001,28,-1,28,1005,28,6,99,0,0,5")
        self.scheduler_bruteforce2_test(intcode, 139629729)
        intcode = ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,"
                   "1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,"
                   "1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,"
                   "-1,56,1005,56,6,99,0,0,0,0,10")
        self.scheduler_bruteforce2_test(intcode, 18216)
# }} end TestSimpleScheduler

# }} end TESTS

def play1():
    with open('input.txt') as file_descriptor:
        line = file_descriptor.readline()
    line = line.strip('\n')
    print(max(bruteforce(line)))

def play2():
    with open('input.txt') as file_descriptor:
        line = file_descriptor.readline()
    line = line.strip('\n')
    print(max(bruteforce(line, "5,6,7,8,9", IntcodeProcessorScheduler2)))

def play3():
    with open('input.txt') as file_descriptor:
        line = file_descriptor.readline()
    line = line.strip('\n')
    intcode = Intcode.fromstr(line)
    #intcode = Intcode(list(map(Opcode, [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99])))
    #intcode = Intcode(list(map(Opcode, [1102,34915192,34915192,7,4,7,99,0])))
    #intcode = Intcode(list(map(Opcode, [104,1125899906842624,99])))
    processor = IntcodeProcessor('A', intcode)
    processor.input.write(Signal(1))
    #processor.output = list()
    processor.process()
    #print(processor.output())

if __name__ == '__main__':
    init_logging()
    #unittest.main()
    #play1()
    #play2()
    play3()
