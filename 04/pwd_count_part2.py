
def increasing_digits(number):
    return all( d1 <= d2 for d1, d2 in zip(number[:-1], number[1:]))

def have_adjacent_equal(number):
    return any( d1 == d2 and d1 * 3 not in number for d1, d2 in zip(number[:-1], number[1:]))

def run(min_number, max_number):
    numbers = range(min_number, max_number + 1)
    numbers = map(str, numbers)
    numbers = filter(increasing_digits, numbers)
    numbers = filter(have_adjacent_equal, numbers)
    numbers = ( 1 for n in numbers )
    return sum(numbers)

print(run(108457, 562041))
