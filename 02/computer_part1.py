import math
import typing

Opcode = typing.NewType('Opcode', int)
Cursor = typing.NewType('Cursor', int)
Intcode = typing.List[int]

class BaseOperator:
    def __init__(self, intcode: Intcode, cursor: Cursor):
        self.intcode = intcode
        self.cursor = cursor

    def get_ptrvalue(self, offset: int) -> Opcode:
        cursor = self.intcode[self.cursor + offset]
        return self.intcode[cursor]

    def set_ptrvalue(self, offset: int, new_opcode: Opcode):
        cursor = self.intcode[self.cursor + offset]
        self.intcode[cursor] = new_opcode

    def process(self):
        pass

class AddOperator(BaseOperator):
    def process(self):
        self.set_ptrvalue(3, self.get_ptrvalue(1) + self.get_ptrvalue(2))

class MulOperator(BaseOperator):
    def process(self):
        self.set_ptrvalue(3, self.get_ptrvalue(1) * self.get_ptrvalue(2))

OPCODES = {
    Opcode(1): AddOperator,
    Opcode(2): MulOperator,
}

class IntcodeProcessor:
    def __init__(self, intcode: Intcode):
        self.intcode = intcode
        self.cursor_idx = 0

    def process(self):
        while True:
            opcode = self.intcode[self.cursor_idx]
            if opcode == 99:
                break
            else:
                operator = OPCODES[opcode]
                operator = operator(self.intcode, self.cursor_idx)
                operator.process()
                self.cursor_idx += 4

def test(initial_intcode: Intcode, expected_intcode: Intcode):
    processor = IntcodeProcessor(initial_intcode[:])
    processor.process()
    final_intcode = processor.intcode
    print(f"process({initial_intcode})={final_intcode} =? {expected_intcode} : ", end='')
    if final_intcode == expected_intcode:
        print('ok')
    else:
        print("fail")

def example():
    test([1,9,10,3,2,3,11,0,99,30,40,50], [3500,9,10,70,2,3,11,0,99,30,40,50])
    test([1, 0, 0, 0, 99], [2, 0, 0, 0, 99])
    test([2, 3, 0, 3, 99], [2, 3, 0, 6, 99])
    test([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801])
    test([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99])

def computer_adaptator(intcode): #: Intcode):
    intcode[1] = 12
    intcode[2] = 2
    processor = IntcodeProcessor(intcode)
    processor.process()
    return processor.intcode[0]

def play():
    with open('input.txt') as fd:
        line = fd.readline()
    line = line.strip()
    line = line.split(',')
    line = map(int, line)
    line = computer_adaptator(list(line))
    print(line)

if __name__ == '__main__':
    #example()
    play()
    
