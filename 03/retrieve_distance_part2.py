import typing

XPos = typing.NewType('X', int)
YPos = typing.NewType('Y', int)
Point = typing.NamedTuple('Point', (
    ('x', XPos),
    ('y', YPos),
))

class Point:
    def __init__(self, x: XPos, y: YPos):
        self.x = x
        self.y = y

    def __str__(self):
        return f'({self.x}, {self.y})'

    def __repr__(self):
        return f'({self.x}, {self.y})'

    def add(self, other_point: Point) -> Point:
        return Point(self.x + other_point.x, self.y + other_point.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other_point):
        return self.x == other_point.x and self.y == other_point.y

class PointDistant(Point):
    def __init__(self, x: XPos, y: YPos, distance: int = 0):
        super().__init__(x, y)
        self.distance = distance

    def add(self, other_point: Point):
        return PointDistant(self.x + other_point.x, self.y + other_point.y, self.distance + 1)


Direction = typing.NewType('Direction', str)
DIRECTIONS = {
    Direction('U'): Point(0, -1),
    Direction('R'): Point(1, 0),
    Direction('L'): Point(-1, 0),
    Direction('D'): Point(0, 1),
}

class Instruction:
    @classmethod
    def fromstr(cls, instruction_string):
        direction = Direction(instruction_string[0])
        assert direction in 'URDL', f'direction {direction} is not known'
        return cls(instruction_string[0], int(instruction_string[1:]))

    def __init__(self, direction: Direction, length: int):
        self.direction = direction
        self.length = length

    def __str__(self):
        return f'{self.direction}{self.length}'

    def iter_points(self, initial_point) -> typing.Iterator[Point]:
        directional_point = DIRECTIONS[self.direction]
        current_point = initial_point
        for _ in range(self.length):
            current_point = current_point.add(directional_point)
            yield current_point


class Path:
    @classmethod
    def fromstr(cls, instructions_string):
        instructions = instructions_string.split(',')
        instructions = map(Instruction.fromstr, instructions)
        return cls(list(instructions))

    def __init__(self, instructions: typing.List[Instruction]):
        self.instructions = instructions

    def length(self) -> int:
        return sum(map(lambda instruction: instruction.length, self.instructions))

    def iter_points(self, initial_point) -> typing.Iterator[Point]:
        pseudo_initial_point = initial_point
        for instruction in self.instructions:
            for point in instruction.iter_points(pseudo_initial_point):
                yield point
            pseudo_initial_point = point

def identify_pathcross(path1: Path, path2: Path) -> int:
    path1_points = list(path1.iter_points(PointDistant(0, 0)))
    path2_points = list(path2.iter_points(PointDistant(0, 0)))
    common_points = set(path1_points).intersection(set(path2_points))
    distances = dict()
    for point in path1_points:
        if point in common_points:
            distances[point] = point.distance
    for point in path2_points:
        if point in common_points:
            distances[point] += point.distance
    return min(distances.values())

def shortcut(path1, path2):
    return identify_pathcross(Path.fromstr(path1), Path.fromstr(path2))

def test(path1: Path, path2: Path, expected_distance: int):
    distance = shortcut(path1, path2)
    print(f"shortcut({path1}, {path2})")
    print(f"={distance} =? {expected_distance} : ", end='')
    if distance == expected_distance:
        print('ok')
    else:
        print("fail")

def example():
    test("R8,U5,L5,D3", "U7,R6,D4,L4", 6)
    test("R75,D30,R83,U83,L12,D49,R71,U7,L72",
            "U62,R66,U55,R34,D71,R55,D58,R83", 159)
    test("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7", 135)

def play():
    with open('input.txt') as fd:
        path1 = fd.readline()
        path2 = fd.readline()
    print(shortcut(path1, path2))

if __name__ == '__main__':
    #example()
    play()
    
