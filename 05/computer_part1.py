import math
import typing

Opcode = typing.NewType('Opcode', int)
Cursor = typing.NewType('Cursor', int)
Intcode = typing.List[Opcode]


class BaseOperator:
    def __init__(self, intcode: Intcode, cursor: Cursor):
        self.intcode = intcode
        self.cursor = cursor
        self.modes = [ 0 ] * self.__class__.NB_ARGS

    def is_position_mode(self, offset: int) -> bool:
        return self.modes[offset - 1] == 0

    def get_argvalue(self, offset: int) -> Opcode:
        if self.is_position_mode(offset):
            cursor = self.intcode[self.cursor + offset]
            return self.intcode[cursor]
        else:
            return self.intcode[self.cursor + offset]

    def set_argvalue(self, offset: int, new_opcode: Opcode):
        if self.is_position_mode(offset):
            cursor = self.intcode[self.cursor + offset]
            self.intcode[cursor] = new_opcode
        else:
            self.intcode[self.cursor + offset] = new_opcode

    def process(self):
        pass

    @property
    def final_cursor(self):
        return self.cursor + self.__class__.NB_ARGS + 1

class AddOperator(BaseOperator):
    NB_ARGS = 3

    def process(self):
        self.set_argvalue(3, self.get_argvalue(1) + self.get_argvalue(2))

class MulOperator(BaseOperator):
    NB_ARGS = 3

    def process(self):
        self.set_argvalue(3, self.get_argvalue(1) * self.get_argvalue(2))

class InputOperator(BaseOperator):
    NB_ARGS = 1

    def process(self):
        self.set_argvalue(1, 1)

class OutputOperator(BaseOperator):
    NB_ARGS = 1

    def process(self):
        print(self.get_argvalue(1))

OPCODES = {
    Opcode(1): AddOperator,
    Opcode(2): MulOperator,
    Opcode(3): InputOperator,
    Opcode(4): OutputOperator,
}

class IntcodeProcessor:
    def __init__(self, intcode: Intcode):
        self.intcode = intcode
        self.cursor_idx = 0

    def process(self):
        while True:
            opcode = self.intcode[self.cursor_idx]
            modes = opcode // 100
            opcode = opcode % 100
            if opcode == 99:
                break
            else:
                operator = OPCODES[opcode]
                operator = operator(self.intcode, self.cursor_idx)
                for idx, digit in enumerate(reversed(str(modes))):
                    operator.modes[idx] = int(digit)
                operator.process()
                self.cursor_idx = operator.final_cursor

def test(initial_intcode: Intcode, expected_intcode: Intcode):
    processor = IntcodeProcessor(initial_intcode[:])
    processor.process()
    final_intcode = processor.intcode
    print(f"process({initial_intcode})={final_intcode} =? {expected_intcode} : ", end='')
    if final_intcode == expected_intcode:
        print('ok')
    else:
        print("fail")

def example():
    test([1,9,10,3,2,3,11,0,99,30,40,50], [3500,9,10,70,2,3,11,0,99,30,40,50])
    test([1, 0, 0, 0, 99], [2, 0, 0, 0, 99])
    test([2, 3, 0, 3, 99], [2, 3, 0, 6, 99])
    test([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801])
    test([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99])

def computer_adaptator(intcode: Intcode):
    processor = IntcodeProcessor(intcode)
    processor.process()

def play():
    with open('input.txt') as fd:
        line = fd.readline()
    line = line.strip()
    line = line.split(',')
    line = map(int, line)
    line = computer_adaptator(list(line))

if __name__ == '__main__':
    #example()
    play()
    
