import itertools
import typing

Opcode = typing.NewType('Opcode', int)
Cursor = typing.NewType('Cursor', int)
Intcode = typing.List[Opcode]

class BaseOperator:
    def __init__(self, processor: 'IntcodeProcessor'):
        self.processor = processor
        self.modes = [ 0 ] * self.__class__.NB_ARGS
        self.self_instruction_is_modified = False

    def is_position_mode(self, offset: int) -> bool:
        return self.modes[offset - 1] == 0

    def get_argvalue(self, offset: int) -> Opcode:
        if self.is_position_mode(offset):
            cursor = self.processor.intcode[self.processor.cursor + offset]
            return self.processor.intcode[cursor]
        else:
            return self.processor.intcode[self.processor.cursor + offset]

    def set_argvalue(self, offset: int, new_opcode: Opcode):
        if self.is_position_mode(offset):
            cursor = self.processor.intcode[self.processor.cursor + offset]
            self.processor.intcode[cursor] = new_opcode
            if cursor == self.processor.cursor:
                self.self_instruction_is_modified = True
        else:
            self.processor.intcode[self.processor.cursor + offset] = new_opcode

    def process(self):
        pass

    def update_cursor(self):
        if not self.self_instruction_is_modified:
            self.processor.cursor = self.processor.cursor + self.__class__.NB_ARGS + 1

    def verbose_getarg(self, offset: int):
        cursor = self.processor.intcode[self.processor.cursor + offset]
        if self.is_position_mode(offset):
            value = self.get_argvalue(offset)
            return f'(@@p{offset}=@@{self.processor.cursor + offset}=@{cursor}={value})'
        else:
            return f'(@p{offset}=@{self.processor.cursor + offset}={cursor})'

    def verbose_setarg(self, offset: int):
        cursor = self.processor.intcode[self.processor.cursor + offset]
        if self.is_position_mode(offset):
            return f'(@@p{offset}=@@{self.processor.cursor + offset}=@{cursor}) <= '
        else:
            return f'(@p{offset}=@{self.processor.cursor + offset}) <= '

    def __str__(self):
        return 'noop'

class BinaryOperator(BaseOperator):
    NB_ARGS = 3

    def process(self):
        self.set_argvalue(3, self.get_argvalue(1) + self.get_argvalue(2))

    def __str__(self):
        return self.verbose_setarg(3) + self.verbose_getarg(1) + ' {} '.format(self.__class__.OPERATOR) + self.verbose_getarg(2)


class AddOperator(BinaryOperator):
    OPERATOR = '+'

    def process(self):
        self.set_argvalue(3, self.get_argvalue(1) + self.get_argvalue(2))

class MulOperator(BinaryOperator):
    OPERATOR= '×'

    def process(self):
        self.set_argvalue(3, self.get_argvalue(1) * self.get_argvalue(2))

class InputOperator(BaseOperator):
    NB_ARGS = 1

    def process(self):
        self.set_argvalue(1, self.processor.input.pop(0))

    def __str__(self):
        return '{} {} (input)'.format(self.verbose_setarg(1), self.processor.input[0])

class OutputOperator(BaseOperator):
    NB_ARGS = 1

    def process(self):
        self.processor.output.append(self.get_argvalue(1))

    def __str__(self):
        return 'output {} '.format(self.verbose_getarg(1))

class JumpIfOperator(BaseOperator):
    NB_ARGS = 2

    def update_cursor(self):
        if self.jump_on_true():
            self.processor.cursor = self.get_argvalue(2)
        else:
            super().update_cursor()

    def __str__(self):
        return 'jmp {} if {} {} 0'.format(self.verbose_getarg(2), self.verbose_getarg(1), self.__class__.OPERATOR)

class JumpIfTrueOperator(JumpIfOperator):
    OPERATOR = '!='

    def jump_on_true(self) -> bool:
        return self.get_argvalue(1) != 0

class JumpIfFalseOperator(JumpIfOperator):
    OPERATOR = '=='

    def jump_on_true(self) -> bool:
        return self.get_argvalue(1) == 0

class ComparatorOperator(BaseOperator):
    NB_ARGS = 3

    def process(self):
        if self.cmp_is_true():
            self.set_argvalue(3, 1)
        else:
            self.set_argvalue(3, 0)

    def __str__(self):
        return '{} 1 if {} {} {} else 0'.format(self.verbose_setarg(3), self.verbose_getarg(1), self.__class__.OPERATOR, self.verbose_getarg(2))

class LessThanOperator(ComparatorOperator):
    OPERATOR = '<'

    def cmp_is_true(self) -> bool:
        return self.get_argvalue(1) < self.get_argvalue(2)

class EqualOperator(ComparatorOperator):
    OPERATOR = '=='
    def cmp_is_true(self) -> bool:
        return self.get_argvalue(1) == self.get_argvalue(2)

OPCODES = {
    Opcode(1): AddOperator,
    Opcode(2): MulOperator,
    Opcode(3): InputOperator,
    Opcode(4): OutputOperator,
    Opcode(5): JumpIfTrueOperator,
    Opcode(6): JumpIfFalseOperator,
    Opcode(7): LessThanOperator,
    Opcode(8): EqualOperator,
}

class IntcodeProcessor:
    def __init__(self, intcode: Intcode):
        self.intcode = intcode
        self.cursor = 0
        self.input = list()
        self.output = list()

    def process(self):
        while True:
            opcode = self.intcode[self.cursor]
            modes = opcode // 100
            opcode = opcode % 100
            print(opcode)
            if opcode == 99:
                break
            else:
                operator = OPCODES[opcode]
                operator = operator(self)
                for idx, digit in enumerate(reversed(str(modes))):
                    operator.modes[idx] = int(digit)
                print(operator)
                input()
                operator.process()
                operator.update_cursor()

class Amp:
    def __init__(self, intcode: Intcode, setting_seq: int):
        self.processor = IntcodeProcessor(intcode)
        self.processor.input.append(setting_seq)
        self.parent = None

    def process(self):
        if self.parent:
            self.parent.process()
            self.processor.input.append(self.parent.processor.output[0])
        else:
            self.processor.input.append(0)
        self.processor.process()

class AmpFactory:
    @classmethod
    def fromstr(cls, intcode: str, setting_seq: str):
        intcode = intcode.split(',')
        intcode = list(map(int, intcode))
        setting_seq = setting_seq.split(',')
        setting_seq = list(map(int, setting_seq))
        return AmpFactory(intcode, setting_seq)

    def __init__(self, intcode: Intcode, setting_seq: typing.List[int]):
        prev = None
        self.processors = list()
        for setting in setting_seq:
            processor = Amp(intcode[:], setting)
            if prev:
                processor.parent = prev
            self.processors.append(processor)
            prev = processor

    def process(self):
        self.processors[-1].process()
        return self.processors[-1].processor.output[0]


def test1(intcode: str, sequence: str, expected_result: int):
    factory = AmpFactory.fromstr(intcode, sequence)
    result = factory.process()
    print(f"process({intcode})={result} =? {expected_result} : ", end='')
    if result == expected_result:
        print('ok')
    else:
        print("fail")

def example1():
    test1("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", "4,3,2,1,0", 43210)
    test1("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", "0,1,2,3,4" , 54321)
    test1("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", "1,0,4,3,2" , 65210)

def bruteforce(intcode: str):
    intcode = intcode.split(',')
    intcode = list(map(int, intcode))
    for setting_seq in itertools.permutations("01234"):
        setting_seq = list(map(int, setting_seq))
        factory = AmpFactory(intcode, setting_seq)
        yield factory.process()

def test2(intcode: str, sequence: str, expected_result: int):
    result = max(bruteforce(intcode))
    print(f"process({intcode})={result} =? {expected_result} : ", end='')
    if result == expected_result:
        print('ok')
    else:
        print("fail")


def example2():
    test2("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", "4,3,2,1,0", 43210)
    test2("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", "0,1,2,3,4" , 54321)
    test2("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", "1,0,4,3,2" , 65210)

def play():
    with open('input.txt') as fd:
        line = fd.readline()
    line = line.strip('\n')
    intcode = line.split(',')
    intcode = list(map(int, intcode))
    amp = Amp(intcode, 5)
    amp.process()
    print(amp.processor.output[0])

if __name__ == '__main__':
    #example1()
    #example2()
    play()
    
